﻿using Mul1.Models;

namespace Mul1
{
    class Finder
    { 
        private int[] _TempArray { get; set; } //Array'in ornegi
        private int _PatternLength { get; set; } //Ardisik sayi uzunlugu

        public Finder(int[] tempArray, int patternLength)
        {
            _TempArray = tempArray;
            _PatternLength = patternLength;
        }

        private int IndexOfZero(int start, int end)
        {
            int zeroIndex = -1; //Eger sifir icermiyorsa -1 doner

            for (int i = start; i < end; i++)
            {
                if (_TempArray[i] == 0)
                {
                    zeroIndex = i; //0 iceren en yuksek indisli deger geri dondurulur
                }
            }

            return zeroIndex;
        }

        public Response Multiplication()
        {
            Response response = new Response();

            int start = 0;
            int end = start + _PatternLength; //Pattern uzunlugu eklenerek bakilacak son indis bulunur

            int length = _TempArray.Length; //Dizi uzunlugu alinir
            bool noFound = true;

            while (noFound)
            {
                var gap = end - length; //Aranmak istenen dizinin uzunlugu dizi boyutunu asmamasi icin farkı alindi

                if (gap < 1)
                {
                    int zeroIndex = IndexOfZero(start, end); //Bakilmak istenen alanda 0 var mi ?

                    if (zeroIndex == -1) //0 icermiyor
                    {
                        ulong multpTemp = 1;
                        string values = "";

                        for (int i = start; i < end; i++)
                        {
                            multpTemp *= (ulong)_TempArray[i];

                            values += _TempArray[i] + ", ";
                        }

                        if (response.Multp < multpTemp) //En buyuk carpim sonucunu elde etmek istedigimiz icin buyuk olan sonucu tercih ederiz
                        {
                            response.StartIndex = start;
                            response.EndIndex = end;
                            response.Multp = multpTemp;
                            response.Values = values.Remove(values.Length-2);
                        }

                        start += 1; //Dizide bakilacak desen 1 kaydirildi
                        end = start + _PatternLength; //End kismi Pattern uzunluguna gore eklendi
                    }
                    else
                    {
                        start = zeroIndex + 1; //Sifir degerinin oldugu indisin bir sonrasina kaydirildi
                        end = start + _PatternLength; 
                    }
                }
                else
                {
                    noFound = false; //Dizinin sonuna gelindi
                }
            }

            return response; //Sonuc
        }
    }
}
