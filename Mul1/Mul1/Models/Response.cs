﻿namespace Mul1.Models
{
    public class Response
    {
        public int StartIndex { get; set; } //Bulunan sequence'in baslangic indisi
        public int EndIndex { get; set; } //Bitis indisi
        public ulong Multp { get; set; } //Carpim Sonucu
        public string Values { get; set; } //Degerler
    }
}
