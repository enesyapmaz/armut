﻿using Mul2.Data.Constants.Enums;
using Mul2.Data.Context;
using Mul2.Helpers;
using Mul2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

/*
 * Muzaffer Enes Yapmaz 31.08.2020
 * Sonuclar
 * App
 * -> Fetch Data
 * 
 * DataTable:
 * Last Name | Sales | Country | Quarter | Bonus
 * 
 * Smith     | 16753 | UK      | Qtr 3   |335,06
 * ...
 * 
 * Edit,Add gibi islemler yapılır ve degisiklik yapılan ilgili sütunun rengi turuncu olur. 
 * Sales Sütunu güncellenir ise Bonus sütunu Sales*0.02 ile güncellenir
 * 
 * -> Save 
 * 
 * Yapılan değişiklikler eğer ilgili id veritabanında var ise Veritabanına update yapılır, ilgili id'ye ait bir kayıt yok ise yeni oluşturulur

 */

namespace Mul2
{
    public partial class Form1 : Form
    {
        Context context = new Context(); //Db Context nesnesi

        List<Sale> sales = new List<Sale>(); //Tablo üzerindeki veriler
        Dictionary<int, DataStatus> changedSalesIds = new Dictionary<int, DataStatus>(); // Değişen veya eklenen verilerin tablo üzerindeki sırası ve Durum bilgisi: Edit,Add 
        int addCount = 0; //Toplam eklenen veri sayısı

        public Form1()
        {
            InitializeComponent();
        }

        private void _btnFetchData_Click(object sender, EventArgs e)
        {
            FtpHelper ftpHelper = new FtpHelper(); //Helper nesnemiz oluşturulur
            var dest = ftpHelper.GetData(); //"example-excel.xlsx"; //İlgili excel dosyası FTP sunucusundan indirilir ve dosya yolu alınır

            if (!string.IsNullOrEmpty(dest)) 
            {
                MessageBox.Show("OK");

                ExcelHelper excelHelper = new ExcelHelper(Application.StartupPath + "\\" + dest);
                //Excel dosyasını okumak için oluşturulan ExcelHelper okunur. Constructure metoduna dosya yolu verilir
                var tableCollection = excelHelper.GetData(); //Excel üzerindeki tablo verileri okunur

                _cmbSheet.Items.Clear();

                foreach (DataTable table in tableCollection)
                    _cmbSheet.Items.Add(table.TableName);//Excel üzerindeki Sheet'ler comboBox'a eklenir

                if (_cmbSheet.Items.Count > 0)
                {
                    _cmbSheet.SelectedIndex = 0;
                }

                DataTable dt = tableCollection[_cmbSheet.SelectedItem.ToString()];

                _dtView.DataSource = dt; //Datatable'ın datasource'una kaynak listesi verilir

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sales.Add(new Sale //init olarak tüm veriler Listemize eklenir
                    {
                        Id = int.Parse(_dtView[0, i].Value.ToString()),
                        Lastname = _dtView[1, i].Value.ToString(),
                        Sales = decimal.Parse(_dtView[2, i].Value.ToString()),
                        Country = _dtView[3, i].Value.ToString(),
                        Quarter = _dtView[4, i].Value.ToString()
                    });
                }

                _dtView.Columns[0].Visible = false; //id'bilgileri column[0]'dadır. Gösterilmek istenmediği için gizlenir
                _dtView.Columns[5].ReadOnly = true; //En son sütun ReadOnly yapıldı. Çünkü otomatik hesaplanan bir veri.
                _btnSave.Enabled = true;
            }
            else
                MessageBox.Show("Not OK");
        }
        //Tablo üzerindeki herangi bir sütun editlendikten sonra çalışır
        private void _dtView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var column = _dtView[e.ColumnIndex, e.RowIndex]; //Editlenen hücre
            column.Style.BackColor = Color.Orange; //Hücre Rengi değiştirilir
            var idColumn = _dtView[0, e.RowIndex].Value.ToString(); //İlgili hücrenin bulunduğu satır üzerindeki 0. hücreden id'bilgisi alınır

            int id = 0;

            if (e.ColumnIndex == 2) //Eğer 2 indisli hücre güncellendiyse bu hücre ile birlikte
            {//son hücrede otomatik olarak hesaplanıp güncellenir
                _dtView[5, e.RowIndex].Value = double.Parse(_dtView[2, e.RowIndex].Value.ToString()) * 0.02;
            }

            if (!string.IsNullOrEmpty(idColumn)) //Eğer idColumn boş değilse yeni bir veri değil, eski kayıt güncellenecektir.
            {
                id = int.Parse(idColumn); //id bilgisi alınır
                if (!changedSalesIds.ContainsKey(id)) // Değişiklik olan Id' listesinde değer daha önceden eklenmediyse 
                {
                    changedSalesIds.Add(id, DataStatus.Edited); //ilgili hücrenin bilgisi eklensin
                }
            }
            else
            {
                
                if (!changedSalesIds.ContainsKey(sales.Count + addCount + 1)) //Yeni bir kayıt ekleniyor.
                {
                    _dtView[0, e.RowIndex].Value = sales.Count + (++addCount); //ilgili yeni oluşturulmuş kayın için id oluşturuluyor. ve 0 'indisli hücreye atanıyor
                    changedSalesIds.Add(sales.Count + addCount, DataStatus.Added);// değişikliklerin tutulduğu dictionary listeme Added kaydı ekleniyor
                }
            }

            //TODO:
            //change oldugu zaman ilgili id'ye ait datayı cek ilgili veriyi guncelle ve hucreyi boya. STATUS'unu edited yap
            //eger veri yeni ise listede yoksa yeni ekle status'unu added yap

            //en son save dendigi zaman edited olanlar update yap, yeni eklenini add yap
            //commit oldugu zaman changed dictionary'sini temizle
        }
        //Değişikliklerin Commit edilmesini sağlayan buton
        private void _btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var changedSale in changedSalesIds) //Değişiklik olan hücrelerde gezilir
                {
                    var rowKey = changedSale.Key - 1; //Değişiklik olan satır.
                    if (changedSale.Value == DataStatus.Edited) //ilgili hücre edit mi yapılmış
                    {
                        var id = int.Parse(_dtView[0, rowKey].Value.ToString()); //id bilgisi alındı

                        Sale sale = null;

                        sale = context.Sales.FirstOrDefault(x => x.Id == id); //Veritabanında ilgili id'ye ait kayıt varsa getirilir

                        if (sale == null) sale = new Sale(); //Kayıt yok ise yeni veri nesnesi oluşturulur

                        sale.Lastname = _dtView[1, rowKey].Value.ToString();
                        sale.Sales = decimal.Parse(_dtView[2, rowKey].Value.ToString());
                        sale.Country = _dtView[3, rowKey].Value.ToString();
                        sale.Quarter = _dtView[4, rowKey].Value.ToString();
                        sale.Bonus = double.Parse(_dtView[5, rowKey].Value.ToString());

                        context.Sales.AddOrUpdate(sale); //Eğer ki Database'de kayıt var ise Update' et yok ise Create 'eder.
                    }
                    else if (changedSale.Value == DataStatus.Added) //Yeni bir kayıt eklenmek isteniyor ise
                    {
                        Sale sale = new Sale();

                        sale.Lastname = _dtView[1, rowKey].Value.ToString();
                        sale.Sales = decimal.Parse(_dtView[2, rowKey].Value.ToString());
                        sale.Country = _dtView[3, rowKey].Value.ToString();
                        sale.Quarter = _dtView[4, rowKey].Value.ToString();
                        sale.Bonus = double.Parse(sale.Sales.ToString()) * 0.02;

                        sales.Add(sale);
                        context.Sales.Add(sale); 
                    }
                }

                context.SaveChanges(); //Tüm değişiklikler Database'de Commit'lenir. Eğer hata oluşur ise RollBack yapılır hiç bir kayıt eklenmez
                addCount = 0;
                MessageBox.Show("Operation Successful");
                changedSalesIds.Clear();
            }

            catch (Exception exp)
            {
                MessageBox.Show(exp.Message.ToString());
            }
        }
    }
}
