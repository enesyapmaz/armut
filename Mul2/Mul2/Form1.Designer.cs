﻿namespace Mul2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dtView = new System.Windows.Forms.DataGridView();
            this._btnFetchData = new System.Windows.Forms.Button();
            this._cmbSheet = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._dtView)).BeginInit();
            this.SuspendLayout();
            // 
            // _dtView
            // 
            this._dtView.AllowUserToDeleteRows = false;
            this._dtView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._dtView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._dtView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dtView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dtView.Location = new System.Drawing.Point(3, 33);
            this._dtView.Name = "_dtView";
            this._dtView.Size = new System.Drawing.Size(568, 386);
            this._dtView.TabIndex = 0;
            this._dtView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._dtView_CellEndEdit);
            // 
            // _btnFetchData
            // 
            this._btnFetchData.Location = new System.Drawing.Point(3, 4);
            this._btnFetchData.Name = "_btnFetchData";
            this._btnFetchData.Size = new System.Drawing.Size(75, 23);
            this._btnFetchData.TabIndex = 3;
            this._btnFetchData.Text = "Fetch Data";
            this._btnFetchData.UseVisualStyleBackColor = true;
            this._btnFetchData.Click += new System.EventHandler(this._btnFetchData_Click);
            // 
            // _cmbSheet
            // 
            this._cmbSheet.FormattingEnabled = true;
            this._cmbSheet.Location = new System.Drawing.Point(41, 425);
            this._cmbSheet.Name = "_cmbSheet";
            this._cmbSheet.Size = new System.Drawing.Size(121, 21);
            this._cmbSheet.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 428);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Sheet";
            // 
            // _btnSave
            // 
            this._btnSave.Enabled = false;
            this._btnSave.Location = new System.Drawing.Point(496, 423);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(75, 23);
            this._btnSave.TabIndex = 6;
            this._btnSave.Text = "Save";
            this._btnSave.UseVisualStyleBackColor = true;
            this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 457);
            this.Controls.Add(this._btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._cmbSheet);
            this.Controls.Add(this._btnFetchData);
            this.Controls.Add(this._dtView);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this._dtView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _dtView;
        private System.Windows.Forms.Button _btnFetchData;
        private System.Windows.Forms.ComboBox _cmbSheet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _btnSave;
    }
}

