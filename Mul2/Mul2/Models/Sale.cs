﻿namespace Mul2.Models
{//Veritabanı Nesnesinin örneği
    public class Sale
    {
        public int Id { get; set; }
        public string Lastname { get; set; }
        public decimal Sales { get; set; }
        public string Country { get; set; }
        public string Quarter { get; set; }
        public double Bonus { get; set; }
    }
}
