﻿using System;
using System.IO;
using System.Net;

namespace Mul2.Helpers
{
    public class FtpHelper
    {
        //Uzak FTP sunucusundan dosya download etmek için kullanılan Helper
        public string GetData()
        {
            try
            {
                String RemoteFtpPath = "ftp://files.000webhost.com:21/public_html/example-excel.xlsx";
                String LocalDestinationPath = "example-excel.xlsx";
                String Username = "yapmazenes";
                String Password = "Ey123456789+";
                Boolean UseBinary = true; // use true for .zip file or false for a text file
                Boolean UsePassive = false;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(RemoteFtpPath);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.KeepAlive = true;
                request.UsePassive = UsePassive;
                request.UseBinary = UseBinary;

                request.Credentials = new NetworkCredential(Username, Password);

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                using (FileStream writer = new FileStream(LocalDestinationPath, FileMode.Create))
                {

                    long length = response.ContentLength;
                    int bufferSize = 2048;
                    int readCount;
                    byte[] buffer = new byte[2048]; //Gelen veri Buffer'da tutulur

                    readCount = responseStream.Read(buffer, 0, bufferSize);
                    while (readCount > 0)
                    {
                        writer.Write(buffer, 0, readCount); //Buffer dizisine, başlangıcından itibaren ilgili data yerleştirilir
                        readCount = responseStream.Read(buffer, 0, bufferSize);
                    }
                }

                reader.Close();
                response.Close();

                return LocalDestinationPath;
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }
    }
}
