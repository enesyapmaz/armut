﻿using ExcelDataReader;
using System.Data;
using System.IO;

namespace Mul2.Helpers
{
    public class ExcelHelper
    {
        private string DestFile { get; set; }

        public ExcelHelper(string destFile)
        {
            DestFile = destFile;
        }
        //Excel Dosyasından Tablo verilerini elde etmek için kullanılan helper
        public DataTableCollection GetData()
        {
            using (var stream = File.Open(DestFile, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                {
                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                    });

                    return result.Tables;
                }
            }
        }
    }
}
