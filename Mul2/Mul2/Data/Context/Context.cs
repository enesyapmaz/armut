﻿using Mul2.Models;
using MySql.Data.EntityFramework;
using System.Data.Entity;

namespace Mul2.Data.Context
{//EntityFramework Mysql DbContext
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class Context : DbContext
    {
        public DbSet<Sale> Sales { get; set; }
    }
}
